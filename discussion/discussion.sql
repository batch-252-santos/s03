
-- [SECTION] Creating new record/s

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");


INSERT albums (album_title, date_released, artist_id) VALUES ("Psy-6", "2012-1-1", 2);
INSERT albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 319, "OPM", 2);


-- [SECTION] Retrieving records from a table
SELECT song_name, genre FROM songs;

-- Retrieve all recrods from the song table
SELECT * FROM songs;

-- Retrieve records with a condition
SELECT song_name FROM songs WHERE genre = "OPM";

SELECT song_name, length FROM songs WHERE length < 310 AND (genre = "OPM" OR genre = "K-Pop");


-- [SECTION] Updating an existing record from a table
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";
# currently using non case-senstive declaration so using song_name: KUNDIMAN with differenct case will still work


-- [SECTION] Deleting an existing record in the table
DELETE FROM songs where genre = "OPM" AND length > 240;